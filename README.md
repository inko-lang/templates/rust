# Project name

TODO:

- Correct the project name in `Cargo.toml`
- Correct the project name in `.gitlab-ci.yml`
- Add the GitLab project ID to `Makefile`
- Correct the project name in `Makefile` (replace the mentions of TODO with the
  name)

## License

All source code in this repository is licensed under the Mozilla Public License
version 2.0, unless stated otherwise. A copy of this license can be found in the
file "LICENSE".
